//Created by Rehan - 081295955149
package main

import (
	"fmt"
	"strconv"
)

func printslice(slice []int) {
	fmt.Println("number --> total")
}

func count_sama(list []int) map[int]int {
	frekuensi_sama := make(map[int]int)
	for _, item := range list {
		_, exist := frekuensi_sama[item]
		if exist {
			frekuensi_sama[item] += 1
		} else {
			frekuensi_sama[item] = 1
		}
	}
	return frekuensi_sama
}

func main() {
	datanya := []int{4, 6, 3, 5, 4, 6, 7, 8, 3, 4, 6, 7, 5, 4, 6, 4, 4, 5, 6}
	printslice(datanya)
	map_sama := count_sama(datanya)
	//fmt.Println(map_sama)
	for k, v := range map_sama {
		fmt.Printf("%s --> %d\n", strconv.Itoa(k), v)
	}
}
